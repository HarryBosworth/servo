# Servo

A library to control servo's from the raspberry pi in Go that is designed to be easy to use and veritile.

## How to use

1. Wrap a pin. (This library uses [periph.io](https://periph.io/) for pin access)
```go
//This example uses pin 23 but any can be used. Even non pwm pins.
pin := servo.New(gpioreg.ByNumber(23))
```

2. Start the pin PWM
```go
pin.Start()
```

3. Control the servo with the Set method
```go
//Set the pin to 45 degrees
pin.Set(45)
```

4. When done stop the PWM output with Stop
```go
pin.Stop()
```

## More Advanced stuff

If you want lower level control of the output you can change the function which defines the pulse width based on the arguments to set:

```go
pin := servo.New(gpioreg.ByNumber(23))

pin.Start()
defer pin.Stop()

//The arguments passed to set are now interpreted as the time the signal should remain on for.
//See the How Servos Work section for more info on what exatly this is doing.
pin.SetFunc(servo.ByOnTimeDelay)
pin.Set(2250 * time.Microsecond) //Set the pulse width to 2250µs or roughtly 180 degrees

//ByOnTimeDelay supports multiple argument types.
//All of the following are equivilent:
pin.Set(2250)
pin.Set("2250µs")
pin.Set("2.25ms")
pin.Set("2250")
```
ByDefault the function used is ByDegrees which sets the delay so that the servo arm adjusts the the angle passed to Set().

SetFuncs have the signiture:
```go
func(*Servo, ...interface{}) error
```

Where the first argument is the Servo object to modify and the second is the data passed to Set

You can even make your on SetFunc:
```go
func byRadians(s *Servo, data ...interface{}) error {
    if len(data) < 1 {
        //No Args passed
        //Handle error
    }

    rads, ok := data[0].(float64)
    if !ok {
        //first arg isn't a float64
        //Handle Error
    }

    //Convert radians to degrees
    degs := rads * (180 / math.Pi)

    //Use the ByDegree function to set the servo
    return servo.ByDegrees(degs)
}
```

## How Servos work

Just a little note on how servo control works in case you wanted a better understanding. (And to remind me when I inevitably forget)

Servos take a signal in the form  of PWM (Pulse Width Modulation). PWM is when a device is controlled by how long a signal stays on for. Servos, typically, take a signal in the range 450µs
 to 2500µs where 450µs is the least the servo can rotate and 2500µs is the most it can rotate. The amount of time the signal stays off for afterwards doesn't effect the rotation of the servo but the longer it is the lower the servo will turn and the less torque it will produce.

 In this library the length of this signal is stored in servo.Servo.OnTime. This can be set manually however, it usually more convient to use a function to translate someother measurment into a delay. e.g. 90° is more meaningfull than 1350µs. 
