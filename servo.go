package servo

import (
	"time"

	"fmt"

	"strconv"

	"strings"

	"periph.io/x/periph/conn/gpio"
)

//Servo hold data to control a servo on a particular pin.
type Servo struct {
	Pin     gpio.PinOut
	OnTime  time.Duration
	OffTime time.Duration

	setFunc func(*Servo, ...interface{}) error
	done    chan struct{}
}

//New creates a new servo pin
func New(pin gpio.PinOut) *Servo {
	servo := &Servo{
		Pin:     pin,
		OnTime:  2250 * time.Microsecond,
		OffTime: 10000 * time.Microsecond,

		setFunc: ByDegrees,
		done:    make(chan struct{}),
	}

	servos[servo.Pin.Number()] = servo

	return servo
}

//Start starts a loop setting the pin to high for servo.OnTime then off for servo.OffTime
//It stops when servo.shouldStop is set to true.
func (servo *Servo) Start() {
	go func() {
	servoLoop:
		for {
			select {
			case <-servo.done:
				break servoLoop
			default:
				servo.Pin.Out(gpio.High)
				time.Sleep(servo.OnTime)
				servo.Pin.Out(gpio.Low)
				time.Sleep(servo.OffTime)
			}
		}
	}()
}

//Stop stops the servo loop from running
func (servo *Servo) Stop() {
	servo.done <- struct{}{}
}

//SetFunc defines how the value passed to set will change the servo.
//By default it sets the servo the number of degrees passed.
func (servo *Servo) SetFunc(f func(*Servo, ...interface{}) error) {
	servo.setFunc = f
}

//Set s the servo using the provided data
func (servo *Servo) Set(data ...interface{}) error {
	return servo.setFunc(servo, data...)
}

//ByDegrees sets the servo to by so many degrees
func ByDegrees(s *Servo, data ...interface{}) error {
	if len(data) < 1 {
		return fmt.Errorf("ByDegrees SetFunc needs one are. The number of degrees to set the servo to")
	}

	var degs float64
	switch data[0].(type) {
	case float64:
		degs = data[0].(float64)
	case float32:
		degs = (float64)(data[0].(float32))
	case int:
		degs = (float64)(data[0].(int))
	case string:
		var err error
		degs, err = strconv.ParseFloat(data[0].(string), 64)
		if err != nil {
			return fmt.Errorf("ByDegress(%s)--Error parsing degrees", data[0].(string))
		}
	}

	nMicro := int64(450 + (((2250 - 450) / 180) * degs))
	s.setOnTime((time.Duration)(nMicro) * time.Microsecond)

	return nil
}

//ByOnTimeDelay sets the angle of the servo to a specific delay.
//if a number less than 2500 is passed it is multiplied by time.Microsecond as set as ontime
//if a string containing usmech is passed it is assumed to be a string representation of a duration and is parsed to time.ParseDuration
//if a time.Duration is passed it is set as the ontime
func ByOnTimeDelay(s *Servo, data ...interface{}) error {
	if len(data) < 1 {
		return fmt.Errorf("ByOnTimeDelay SetFunc needs atleast one argument")
	}

	var del time.Duration
	switch data[0].(type) {
	case string:
		if !strings.ContainsAny(data[0].(string), "usmech") {
			//Assume the string passed was just a number and not a duration
			n, err := strconv.ParseInt(data[0].(string), 10, 64)
			if err != nil {
				return fmt.Errorf("Error parsinmg arg[0] %s as int: %v", data[0], err)
			}

			del = parseIntAsDur(n)
		} else {
			var err error
			del, err = time.ParseDuration(data[0].(string))
			if err != nil {
				return fmt.Errorf("Error parsing %s as duration: %v", data[0].(string), err)
			}
		}
	case int64:
		del = parseIntAsDur(data[0].(int64))
	case int:
		del = parseIntAsDur(int64(data[0].(int)))
	case time.Duration:
		del = data[0].(time.Duration)
	}

	s.setOnTime(del)

	return nil
}

func parseIntAsDur(i int64) time.Duration {
	if i <= 2500 {
		return time.Duration(i) * time.Microsecond
	}

	return time.Duration(i)
}

func (servo *Servo) setOnTime(delay time.Duration) {
	switch {
	case delay < 450*time.Microsecond:
		delay = 450 * time.Microsecond
	case delay > 2500*time.Microsecond:
		delay = 2500 * time.Microsecond
	}

	servo.OnTime = delay
}

var servos = make(map[int]*Servo)
